let featuresElement = document.getElementById('featuresElement'); 
let laptopHeadingElement = document.getElementById('laptopHeadingElement');
let laptopDescriptionElement = document.getElementById('laptopDescriptionElement');
let laptopPriceElement = document.getElementById('laptopPriceElement');
let laptopImageElement = document.getElementById('laptopImageElement');
let laptopMessageElement = document.getElementById('laptopMessageElement');

export function showSelectedLaptop(laptop) {   
    let specsString = "";
    laptop.specs.forEach(spec => {
        specsString += spec + "<br>" 
    });
    featuresElement.innerHTML = specsString;
    laptopHeadingElement.innerHTML = laptop.title;
    laptopDescriptionElement.innerHTML = laptop.description;
    laptopPriceElement.innerHTML = laptop.price + " NOK";
    laptopImageElement.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/" + laptop.id + ".png";
} 

export function showBuyLaptopError() {    
    laptopMessageElement.style = "color:red"
    laptopMessageElement.innerHTML = "Your balance is too low";
} 

export function showBuyLaptopStatus(balance, laptop) {    
    balanceElement.innerHTML = "Balance " + balance + " kr";
    laptopMessageElement.style = "color:green"
    laptopMessageElement.innerHTML = "Thank you for your purchase. That will be " + laptop.price + " NOK"
}  