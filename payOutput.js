let balanceElement = document.getElementById('balanceElement');
let payElement = document.getElementById('payElement');
let workMessageElement = document.getElementById('workMessageElement');

export function showPayStatus(pay) { 
    payElement.innerHTML = "Pay " + pay + " kr";
    workMessageElement.innerHTML = "Work registered successfully. 100 NOK has been transfered to your pay";
}

export function showTransferToBankStatus(loan,balance,pay) {   
    let loanElement = document.getElementById('loanElement');
    loanElement.innerHTML = "Loan " + loan + " kr";
    balanceElement.innerHTML = "Balance " + balance + " kr";
    payElement.innerHTML = "Pay " + pay + " kr";
    workMessageElement.innerHTML = "Pay transfered successfully.";
} 