let balanceElement = document.getElementById('balanceElement');
let loanElement = document.getElementById('loanElement');
let loanMessageElement = document.getElementById('loanMessageElement');

export function showNewLoanStatus(loan, balance) {
    balanceElement.innerHTML = "Balance " + balance + " kr";
    loanElement.innerHTML = "Loan " + loan + " kr";
    loanMessageElement.style = "color:green"
    loanMessageElement.innerHTML = "Loan registered";
    document.getElementById('getLoanBtnElement').innerHTML = "Repay loan";
}

export function showPayedLoanStatus(balance) {
    balanceElement.innerHTML = "Balance " + balance + " kr";
    loanElement.innerHTML = ".";
    loanMessageElement.style = "color:green"
    loanMessageElement.innerHTML = "Loan payed successfully";
    document.getElementById('getLoanBtnElement').innerHTML = "Get a loan";
}