import {Acount} from "./Acount.js";
import {getLaptops} from "./laptops.js";
import {showPayStatus, showTransferToBankStatus} from "./payOutput.js";
import {showNewLoanStatus, showPayedLoanStatus } from "./loanOutput.js";
import {showLoanInputError, showLoanAmountError, showLoanPayError } from "./loanErrors.js";
import {showSelectedLaptop, showBuyLaptopError, showBuyLaptopStatus} from "./laptopOutput.js";

let laptopsMap = getLaptops();
let acount = new Acount();

document.getElementById('getLoanBtnElement').addEventListener("click",loanButtonClick);
document.getElementById('workBtnElement').addEventListener("click",work);
document.getElementById('bankBtnElement').addEventListener("click",transfer_to_bank);
document.getElementById('laptopsElement').addEventListener("click",select_laptop);
document.getElementById('buyBtnElement').addEventListener("click",buy);

function loanButtonClick() {
    if (acount.loan==0) getLoan();
    else payLoan();
}

function getLoan() {
    let innput = parseInt(prompt("How much do you want to loan?"))
    if (isNaN(innput) || innput<=0) showLoanInputError();
    else if (innput > acount.balance*2) showLoanAmountError();
    else {
        acount.getLoan(innput);
        showNewLoanStatus(acount.loan, acount.balance);
    }
}

function payLoan() {
    if (acount.loan > acount.balance) showLoanPayError();
    else {
        acount.payLoan();
        showPayedLoanStatus(acount.balance);
    }
}

function work() {
    acount.pay += 100;
    showPayStatus(acount.pay); 
}

function transfer_to_bank() {
    acount.transfer_to_bank();
    showTransferToBankStatus(acount.loan,acount.balance,acount.pay);
}

function select_laptop() {
    var e = document.getElementById("laptopsElement");
    var id = parseInt(e.options[e.selectedIndex].value);
    let laptop =  laptopsMap.get(id)
    showSelectedLaptop(laptop);
}

function buy() {
    var e = document.getElementById("laptopsElement");
    var id = parseInt(e.options[e.selectedIndex].value);
    let laptop = laptopsMap.get(id)
    if (laptop.price>acount.balance) showBuyLaptopError();
    else {
        acount.balance -= laptop.price;
        showBuyLaptopStatus(acount.balance,laptop);
    }
}
//