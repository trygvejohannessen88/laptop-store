export function getLaptops(laptops) {
    let laptopsMap = new Map();
    let laptopsElement = document.getElementById('laptopsElement');

    fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMap(laptops))

    function addLaptopsToMap(laptops) {
        laptops.forEach(laptop => addLaptopToMap(laptop)); 
        addLaptopsToMenu(laptops)
    }

    function addLaptopToMap(laptop) {
        laptopsMap.set(laptop.id, laptop);
    }

    function addLaptopsToMenu(laptops) {
        laptops.forEach(laptop => addLaptopToMenu(laptop));
    }

    function addLaptopToMenu(laptop) {
        const laptopElement = document.createElement("option");
        laptopElement.value = laptop.id;
        laptopElement.appendChild(document.createTextNode(laptop.title));
        laptopsElement.appendChild(laptopElement)
    }
    return laptopsMap;
}