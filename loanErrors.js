let loanMessageElement = document.getElementById('loanMessageElement');

export function showLoanInputError() {
    loanMessageElement.style = "color:red"
    loanMessageElement.innerHTML = "Error! Invalid output";
}

export function showLoanAmountError() {
    loanMessageElement.style = "color:red"
    loanMessageElement.innerHTML = "You can not loan more than your balance*2";
}

export function showLoanPayError() {
    loanMessageElement.style = "color:red"
    loanMessageElement.innerHTML = "Your balance is too low";
}